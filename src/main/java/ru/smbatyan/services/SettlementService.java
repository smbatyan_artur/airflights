package ru.smbatyan.services;


import java.util.List;
import java.util.Map;

public interface SettlementService {
    Map<String, Long> getMinimumFlightTime(String departureCityCode,  String arrivalCityCode);

    double getAveragePrice(List<Double> prices);

    double getMedina(Double[] prices);

    double getDifferenceAveragePriceAndMedina(String departureCityCode,  String arrivalCityCode);
}
