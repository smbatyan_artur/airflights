package ru.smbatyan.services;

import org.springframework.stereotype.Component;
import ru.smbatyan.mappers.TimeMapper;


@Component
public class PrintServiceImpl implements PrintService {

    private final SettlementService settlementService;
    private final TimeMapper timeMapper;

    public PrintServiceImpl(SettlementService settlementService, TimeMapper timeMapper) {
        this.settlementService = settlementService;
        this.timeMapper = timeMapper;
    }


    @Override
    public void printInfo() {
        settlementService.getMinimumFlightTime("VVO", "TLV").forEach((s, time) -> {
            String result = "Код авиакомпании IATA -> " + s +
                    " Минимальное время полета  -> " + timeMapper.parse(time);
            System.out.println(result);
        });
        System.out.println("Разница между средней ценой и медианой для полета между городами  Владивосток и Тель-Авив составляет -> " + settlementService.getDifferenceAveragePriceAndMedina("VVO", "TLV"));
    }
}
