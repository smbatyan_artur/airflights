package ru.smbatyan.services;

import org.springframework.stereotype.Component;
import ru.smbatyan.dto.TicketDto;
import ru.smbatyan.mappers.TicketMapper;
import ru.smbatyan.mappers.TimeMapper;
import ru.smbatyan.utils.ResourcesUtil;
import java.util.*;

@Component
public class SettlementServiceImpl implements SettlementService {

    private final TicketMapper ticketMapper;
    private final ResourcesUtil resourcesUtil;
    private final TimeMapper tm;

    public SettlementServiceImpl(TicketMapper ticketMapper, ResourcesUtil resourcesUtil, TimeMapper timeMapper) {
        this.ticketMapper = ticketMapper;
        this.resourcesUtil = resourcesUtil;
        this.tm = timeMapper;
    }


    @Override
    public double getDifferenceAveragePriceAndMedina(String departureCityCode,  String arrivalCityCode) {
        final List<Double> prices = new ArrayList<>();
        getAllTicket().stream().filter(ticketDto -> ticketDto.getOrigin().equals(departureCityCode) && ticketDto.getDestination().equals(arrivalCityCode)).forEach(ticketDto -> prices.add(ticketDto.getPrice()));
        final double averagePrice = getAveragePrice(prices);
        final double medina = getMedina(prices.toArray(Double[]::new));
        return averagePrice - medina;
    }


    private Set<TicketDto> getAllTicket() {
        return ticketMapper.parse(resourcesUtil.getContentFile("tickets.json")).getTickets();
    }

    @Override
    public Map<String, Long> getMinimumFlightTime(String departureCityCode,  String arrivalCityCode) {
        final Map<String, Long> mapResult = new HashMap<>();
        getAllTicket().stream().filter(ticketDto -> ticketDto.getOrigin().equals(departureCityCode) && ticketDto.getDestination().equals(arrivalCityCode)).forEach(ticketDto -> {
            if (!mapResult.containsKey(ticketDto.getCarrier())) {
                mapResult.put(ticketDto.getCarrier(), tm.parse(ticketDto.getArrival_date(), ticketDto.getArrival_time()) - tm.parse(ticketDto.getDeparture_date(), ticketDto.getDeparture_time()));
            } else {
                if (mapResult.get(ticketDto.getCarrier()) > (tm.parse(ticketDto.getArrival_date(), ticketDto.getArrival_time()) - tm.parse(ticketDto.getDeparture_date(), ticketDto.getDeparture_time()))) {
                    mapResult.put(ticketDto.getCarrier(), tm.parse(ticketDto.getArrival_date(), ticketDto.getArrival_time()) - tm.parse(ticketDto.getDeparture_date(), ticketDto.getDeparture_time()));
                }
            }
        });
        return mapResult;
    }


    @Override
    public double getAveragePrice(List<Double> prices) {
        double allSum = 0.0;
        for (Double price : prices)
            allSum = allSum + price;
        return allSum / prices.size();
    }


    @Override
    public double getMedina(Double[] prices) {
        Arrays.sort(prices);
        if (prices.length % 2 != 0) {
            return prices[(prices.length / 2)];
        } else {
            double leftValue = prices[(prices.length / 2) - 1];
            double rightValue = prices[(prices.length / 2)];
            return (leftValue + rightValue) / 2;
        }
    }
}
