package ru.smbatyan.mappers;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Component
public class TimeMapper {

    public long parse(String date, String time){
        LocalDateTime localDateTime = LocalDateTime.parse(date+" "+time, DateTimeFormatter.ofPattern("dd.MM.yy [H][HH]:mm"));
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant()).getTime();
    }

    public String parse(long time){
        final int hours = (int) (time / (60 * 60 * 1000));
        final int minutes = (int) (time / (60 * 1000)) % 60;
        final int seconds = (int) (time / 1000) % 60;
        return hours + ":" + minutes + ":" + seconds;
    }
}
