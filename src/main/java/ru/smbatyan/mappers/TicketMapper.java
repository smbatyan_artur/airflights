package ru.smbatyan.mappers;

import com.google.gson.Gson;
import org.springframework.stereotype.Component;
import ru.smbatyan.dto.ShellTicketsDto;

@Component
public class TicketMapper {

    private final Gson gson;

    public TicketMapper(Gson gson) {
        this.gson = gson;
    }

    public ShellTicketsDto parse(String json) {
        return gson.fromJson(json, ShellTicketsDto.class);
    }
}
