package ru.smbatyan;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.smbatyan.config.SpringConf;
import ru.smbatyan.services.PrintService;
import ru.smbatyan.services.PrintServiceImpl;


public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConf.class);
        PrintService printService = context.getBean(PrintServiceImpl.class);
        printService.printInfo();


    }
}