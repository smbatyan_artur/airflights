package ru.smbatyan.dto;

import java.util.Objects;

public class TicketDto {
    private String origin;
    private String origin_name;
    private String destination;
    private String destination_name;
    private String departure_date;
    private String departure_time;
    private String arrival_date;
    private String arrival_time;
    private String carrier;
    private int stopsl;
    private double price;


    public TicketDto(String origin, String origin_name, String destination, String destination_name, String departure_date, String departure_time, String arrival_date, String arrival_time, String carrier, int stopsl, double price) {
        this.origin = origin;
        this.origin_name = origin_name;
        this.destination = destination;
        this.destination_name = destination_name;
        this.departure_date = departure_date;
        this.departure_time = departure_time;
        this.arrival_date = arrival_date;
        this.arrival_time = arrival_time;
        this.carrier = carrier;
        this.stopsl = stopsl;
        this.price = price;
    }

    public TicketDto() {
    }


    public String getOrigin() {
        return origin;
    }

    public String getOrigin_name() {
        return origin_name;
    }

    public String getDestination() {
        return destination;
    }

    public String getDestination_name() {
        return destination_name;
    }

    public String getDeparture_date() {
        return departure_date;
    }

    public String getDeparture_time() {
        return departure_time;
    }

    public String getArrival_date() {
        return arrival_date;
    }

    public String getArrival_time() {
        return arrival_time;
    }

    public String getCarrier() {
        return carrier;
    }

    public int getStopsl() {
        return stopsl;
    }

    public double getPrice() {
        return price;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TicketDto ticketDto = (TicketDto) o;
        return stopsl == ticketDto.stopsl && Double.compare(price, ticketDto.price) == 0 && Objects.equals(origin, ticketDto.origin) && Objects.equals(origin_name, ticketDto.origin_name) && Objects.equals(destination, ticketDto.destination) && Objects.equals(destination_name, ticketDto.destination_name) && Objects.equals(departure_date, ticketDto.departure_date) && Objects.equals(departure_time, ticketDto.departure_time) && Objects.equals(arrival_date, ticketDto.arrival_date) && Objects.equals(arrival_time, ticketDto.arrival_time) && Objects.equals(carrier, ticketDto.carrier);
    }

    @Override
    public int hashCode() {
        return Objects.hash(origin, origin_name, destination, destination_name, departure_date, departure_time, arrival_date, arrival_time, carrier, stopsl, price);
    }


    @Override
    public String toString() {
        return "TicketDto{" +
                "origin='" + origin + '\'' +
                ", origin_name='" + origin_name + '\'' +
                ", destination='" + destination + '\'' +
                ", destination_name='" + destination_name + '\'' +
                ", departure_date='" + departure_date + '\'' +
                ", departure_time='" + departure_time + '\'' +
                ", arrival_date='" + arrival_date + '\'' +
                ", arrival_time='" + arrival_time + '\'' +
                ", carrier='" + carrier + '\'' +
                ", stopsl=" + stopsl +
                ", price=" + price +
                '}';
    }
}

