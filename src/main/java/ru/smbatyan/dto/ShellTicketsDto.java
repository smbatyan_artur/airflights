package ru.smbatyan.dto;

import java.util.Set;

public class ShellTicketsDto {
    private Set<TicketDto> tickets;

    public ShellTicketsDto(Set<TicketDto> tickets) {
        this.tickets = tickets;
    }

    public ShellTicketsDto() {
    }

    public Set<TicketDto> getTickets() {
        return tickets;
    }
}
