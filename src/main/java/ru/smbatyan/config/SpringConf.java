package ru.smbatyan.config;

import com.google.gson.Gson;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan(value = "ru.smbatyan")
public class SpringConf {

    @Bean
    public Gson gson(){
        return new Gson();
    }
}
