package ru.smbatyan.utils;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Component
public class ResourcesUtil {

    public String getContentFile(String fileName) {
        StringBuilder json = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getClassLoader().getResource(fileName).openStream()))) {
            String str;
            while ((str = reader.readLine()) != null) json.append(str);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return json.toString();
    }
}
